package ru.t1.schetinin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.dto.request.AbstractUserRequest;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.user.AccessDeniedException;
import ru.t1.schetinin.tm.dto.model.SessionDTO;

@Controller
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    @Autowired
    private IServiceLocator serviceLocator;

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role){
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable SessionDTO session;
        try {
            session = serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request){
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
