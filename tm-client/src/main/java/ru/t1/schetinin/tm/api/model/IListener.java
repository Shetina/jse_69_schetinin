package ru.t1.schetinin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    void execute(ConsoleEvent consoleEvent);

}