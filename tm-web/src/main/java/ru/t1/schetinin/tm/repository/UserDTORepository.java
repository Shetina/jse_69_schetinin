package ru.t1.schetinin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.dto.UserDTO;

@Repository
@Scope("prototype")
public interface UserDTORepository extends JpaRepository<UserDTO, String> {

    @Nullable
    UserDTO findByLogin(@NotNull final String login);

}