package ru.t1.schetinin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.schetinin.tm.api.service.model.IProjectService;
import ru.t1.schetinin.tm.model.dto.CustomUser;
import ru.t1.schetinin.tm.repository.ProjectDTORepository;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) throws Exception {
        return new ModelAndView("project-list", "projects", projectService.findAllByUserId(user.getUserId()));
    }

}